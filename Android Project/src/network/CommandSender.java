package network;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.channels.SocketChannel;
import java.util.PriorityQueue;

import freaky.creations.toleaveremote.AvailablePcs;
import freaky.creations.toleaveremote.AvailablePcs.ArrayPcAvailableAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class CommandSender {
	
	//Begin: Private Data Members
	private String ipAdress;
	private int port;
	private Socket socketClient;
	private BufferedOutputStream outputStream;
	private int timeOut;
	private Activity context;
	
	
	private boolean isFailed;
	private boolean isSendingT;
	//private boolean sendDataState;
	private Thread senderThread;
	
	private String dataToSend;
	
	private ProgressDialog connectingDialog;
	
	private PriorityQueue<String> commandQueue;
	
	//End: Private Data Members
	
	//Begin: Constructors
	
	public CommandSender(Activity context){
		this(context,"192.168.137.1", 10000, 5000);
	}
	
	public CommandSender( Activity contextAct, String ip, int port,int timOutExt){
		SetNetworkParams(ip, port);
		isSendingT = false;
		dataToSend = "";
		//sendDataState = false;
		timeOut = timOutExt;
		context = contextAct;
		isFailed = false;
		
		commandQueue = new PriorityQueue<String>();
		
		connectingDialog=new ProgressDialog(context);
		connectingDialog.setCancelable(true);
		connectingDialog.setMessage("Conecting....");
		connectingDialog.setCancelable(false);
		connectingDialog.setCanceledOnTouchOutside(false);
		connectingDialog.setOnCancelListener(new Dialog.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // DO SOME STUFF HERE
            }
        });
	}
	
	//End: Constructors
	
	//Begin: Public Interface
	
	public void SetNetworkParams(String ip, int port){
		this.ipAdress = ip;
		this.port = port;
	}
	
	public void Start() throws Exception{
		try{
			isSendingT = false;
			senderThread = new Thread(new SenderThread());
			senderThread.start();
			connectingDialog.show();
			isFailed = false;
			Log.d("CONNECT", "Connect");
		}catch(Exception e){
			throw new Exception("Failed Start Command Sender: " + e.getMessage());
		}
	}
	
	public void SendCommand(String command){
		if(isSendingT){
			dataToSend = command;
			commandQueue.add(dataToSend);
			//sendDataState = true;
			//Log.d("SENT", dataToSend);
		}
	}
	
	public void Stop() throws Exception{		
		try {
			if(socketClient != null){				
				if(outputStream != null){
					outputStream.close();
				}
				socketClient.close();
			}	
		} catch (Exception e) {
			throw new Exception("Failed Stop Command Sender: " + e.getMessage());
		}
	}
	
	public boolean IsActive(){
		return isSendingT;
	}
	
	public boolean IsSending(){
		return !commandQueue.isEmpty();
	}
	public boolean IsFailed(){
		return isFailed;
	}
	
	public int getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}
	
	//End: Public Interface

  
	// Thread for the command
  public class SenderThread implements Runnable {
   @Override
   public void run() {
	   try{		  
		   
			socketClient = new Socket();	
			socketClient.setReuseAddress(true);
			socketClient.connect((new InetSocketAddress(ipAdress, port)),timeOut);
			isSendingT = true;
			connectingDialog.dismiss();
	       while(isSendingT){                              
			   if(!commandQueue.isEmpty()){
				   OutputStream os = socketClient.getOutputStream();
				   outputStream = new BufferedOutputStream(os);
				   byte[] buffer = (commandQueue.poll()).getBytes();
				   outputStream.write(buffer, 0, buffer.length);				
				   outputStream.flush();
				   //sendDataState = false;
				   Log.d("FLUSHED",new String(buffer, "UTF-8"));
			   }
	       }
	   }
	   catch(Exception e){
		   if(!isSendingT){
			   connectingDialog.dismiss();
			   Handler mHandler = new Handler(Looper.getMainLooper());
			   mHandler.post(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						 AlertDialog.Builder alertConnection =  new AlertDialog.Builder(context)
						    .setTitle("Conexion Fallida")
						    .setMessage("Verifique que la direccion dada sea correcta, esta conectado a la red, el juego esta ejecutando y el Time Out es el suficiente.")
						    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
						        public void onClick(DialogInterface dialog, int whichButton) {
						        	// Do nothing
						        }
						    });
						   alertConnection.show();
					}
				});		 
		   }
		   isFailed = true;
		   Log.d("Sending Error",e.getMessage());
	   }
   	}
  }
  
  
}
