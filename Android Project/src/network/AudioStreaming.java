package network;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Socket;
import java.net.SocketException;

import network.CommandSender.SenderThread;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

public class AudioStreaming {
	
	//Begin: Global Variables
	
	public static final String REQUEST_STREAMING = "AUST";
	public static final String DISMISS_STREAMING = "DAUST";
	
	static final String LOG_TAG = "UdpAudioStream";
    static final int AUDIO_PORT = 3490;
    static final int SAMPLE_RATE = 48000;
    static final int SAMPLE_INTERVAL = 22; // milliseconds
    static final int SAMPLE_SIZE = 2; // bytes per sample
    //static final int BUF_SIZE = SAMPLE_INTERVAL*SAMPLE_INTERVAL*SAMPLE_SIZE*2;
    static final int BUF_SIZE = 4096;	
	//End: Global Variables
    
    //Begin: Private Data Members
    
    AudioTrack track;
    DatagramSocket udpSocket;
    boolean isPlaying;
    
    //End: Private Data Members
	
	
	
	//Begin: Constructors
	
	public AudioStreaming(){
		isPlaying = false;
	}
	
	//End:Constructors
	
	//Begin: Public Interface
	
	
	public void Play(){
		try{
			isPlaying = true;
			Thread receiverThread = new Thread(new ReceiverThread());
			receiverThread.start();
		}catch(Exception ex){
			Log.e(LOG_TAG, "Error on Playing: " + ex.toString());
		}
	}
	
	public void Pause(){
		try{
			if(track != null){
				track.pause();
				isPlaying = false;
			}
		}catch(Exception ex){
			Log.e(LOG_TAG, "Error on Stop: " + ex.toString());
		}
	}
	
	public void Resume(){
		try{
			if(track != null){
				track.play();
				isPlaying = true;
			}
		}catch(Exception ex){
			Log.e(LOG_TAG, "Error on Stop: " + ex.toString());
		}
	}
	
	
	public void Stop(){
		try{
			if(track != null){
				track.stop();
				track.release();
			}
			if(udpSocket != null){
				udpSocket.close();
			}
			isPlaying = false;
		}catch(Exception ex){
			Log.e(LOG_TAG, "Error on Stop: " + ex.toString());
		}
	}
	
	public boolean IsPlaying(){
		return isPlaying;
	}
	
	//public 
	
	//End: Public Interface
	
	
	//Thread for Streaming
	
	// Thread for the command
	  class ReceiverThread implements Runnable {
	   @Override
	   public void run() {
		   Log.e(LOG_TAG, "start recv thread, thread id: "
               + Thread.currentThread().getId());
           //int bufferSize = AudioTrack.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
           track = new AudioTrack(AudioManager.STREAM_MUSIC, 
                   SAMPLE_RATE, AudioFormat.CHANNEL_OUT_STEREO, 
                   AudioFormat.ENCODING_PCM_16BIT, BUF_SIZE, 
                   AudioTrack.MODE_STREAM);
           track.play();
           try
           {
        	   udpSocket = new DatagramSocket(AUDIO_PORT);
               byte[] buf = new byte[BUF_SIZE];

               while(udpSocket != null )
               {
            	   if(isPlaying){
	                   DatagramPacket pack = new DatagramPacket(buf, BUF_SIZE);
	                   udpSocket.receive(pack);
	                   //Log.d(LOG_TAG, "recv pack: " + pack.getLength());                    
	                   track.write(pack.getData(), 0,pack.getLength());
            	   }
               }
           }
           catch (SocketException se)
           {
               Log.e(LOG_TAG, "SocketException: " + se.toString());
           }
           catch (IOException ie)
           {
               Log.e(LOG_TAG, "IOException" + ie.toString());
           }
	   	}
	  }
}
