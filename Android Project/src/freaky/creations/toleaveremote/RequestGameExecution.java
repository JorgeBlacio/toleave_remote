package freaky.creations.toleaveremote;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RequestGameExecution extends Activity {
	   @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.sc_requestgameexecution);
	        
	        Button buttonContinue = (Button) findViewById(R.id.btnContinue);	
	        buttonContinue.setBackgroundResource(R.drawable.continuar_button_selector);
	        buttonContinue.setOnClickListener(new View.OnClickListener() {
				   public void onClick(View view) {
					   Intent intent = new Intent(RequestGameExecution.this, AvailablePcs.class);
			            startActivity(intent);      
			            finish();
				   }
				  });
	        
	    }
	    
}
