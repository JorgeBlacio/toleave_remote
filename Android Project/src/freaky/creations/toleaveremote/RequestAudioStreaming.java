package freaky.creations.toleaveremote;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import network.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class RequestAudioStreaming extends Activity {

		
		
	   @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.sc_audiostreaming);
	        final ToLeaveRemoteVariables globalVariables = ((ToLeaveRemoteVariables)getApplication());	                
	        
	        Button buttonActiveAudio = (Button) findViewById(R.id.btnActiveAudio);	
	        buttonActiveAudio.setBackgroundResource(R.drawable.activar_audio_selector);
	        buttonActiveAudio.setOnClickListener(new View.OnClickListener() {
				   public void onClick(View view) {	  
					   TurnOnStreamingOnRemote(globalVariables.getCommandSender());
					   globalVariables.setAudioStreaming(new AudioStreaming());
					   globalVariables.getAudioStreaming().Play();
					   CallPhysicalControllerScreen();
				   }
				  });
	        
	        
	        Button buttonIgnore = (Button) findViewById(R.id.btnIgnoreAudio);	
	        buttonIgnore.setBackgroundResource(R.drawable.ignorar_selector);
	        buttonIgnore.setOnClickListener(new View.OnClickListener() {
				   public void onClick(View view) {
					   CallPhysicalControllerScreen();
				   }
				  });	        
	        
	    }
	   
	   private String GetIpAdressPC(){
		   Bundle extras = getIntent().getExtras();
	        String ipAdress = null;
	        if (extras != null) {
	        	ipAdress = extras.getString("ip_adress");
	        }
	        return ipAdress;
	   }
	   
	   private void CallPhysicalControllerScreen(){
		  	   
		   Intent intent = new Intent(RequestAudioStreaming.this, PhysicalController.class);
			intent.putExtra("ip_adress",GetIpAdressPC());
           startActivity(intent);      
           finish();
	   }
	   
	   private void TurnOnStreamingOnRemote(CommandSender commandSender){
		   while(!commandSender.IsActive()){}
		   commandSender.SendCommand(AudioStreaming.REQUEST_STREAMING);
	   }
}
