package freaky.creations.toleaveremote;

import network.*;
import android.app.Application;

public class ToLeaveRemoteVariables extends Application {
	private AudioStreaming audioStreaming;
	private CommandSender commandSender;
	

	public AudioStreaming getAudioStreaming() {
		return audioStreaming;
	}

	public void setAudioStreaming(AudioStreaming audioStreaming) {
		this.audioStreaming = audioStreaming;
	}

	public CommandSender getCommandSender() {
		return commandSender;
	}

	public void setCommandSender(CommandSender commandSender) {
		this.commandSender = commandSender;
	}
	
}
