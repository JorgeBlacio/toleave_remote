package freaky.creations.toleaveremote;

import network.AudioStreaming;
import network.CommandSender;
import Util.FreakyTimer;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ConnectionSuccess extends Activity {
	 protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.sc_completed_connection);
	        
	        
	        FreakyTimer timer = new FreakyTimer(2.0, 
        		new FreakyTimer.ActionOnTimer() {					
					@Override
					public void DoAction() {
						//findViewById(R.id.freakyLogo).setVisibility(View.INVISIBLE);
						Intent intent = new Intent(ConnectionSuccess.this, RequestAudioStreaming.class);
						intent.putExtra("ip_adress",GetIpAdressPC());
			            startActivity(intent);      
			            finish();
					}
				}
    		);
	        timer.Execute();	        
	 }
	 
	 private String GetIpAdressPC(){
		   Bundle extras = getIntent().getExtras();
	        String ipAdress = null;
	        if (extras != null) {
	        	ipAdress = extras.getString("ip_adress");
	        }
	        return ipAdress;
	   }
	        
}
