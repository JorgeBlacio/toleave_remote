package freaky.creations.toleaveremote;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.*;
import Util.*;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_main);
        //findViewById(R.id.freakyLogo).setVisibility(View.INVISIBLE);
        
        FreakyTimer timer = new FreakyTimer(2.0, 
        		new FreakyTimer.ActionOnTimer() {					
					@Override
					public void DoAction() {
						//findViewById(R.id.freakyLogo).setVisibility(View.INVISIBLE);
						Intent intent = new Intent(MainActivity.this, RequestGameExecution.class);
			            startActivity(intent);      
			            finish();
					}
				}
        		);
        timer.Execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_connection, menu);
        return true;
    }
    
}
