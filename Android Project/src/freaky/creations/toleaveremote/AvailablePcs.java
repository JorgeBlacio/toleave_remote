package freaky.creations.toleaveremote;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.regex.Pattern;

import org.apache.http.conn.util.InetAddressUtils;

import network.CommandSender;

import android.R.string;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class AvailablePcs extends ListActivity {
	
	private ArrayList<PcInfo> items;
	private ProgressDialog refreshingDialog;
	private ToLeaveRemoteVariables globalVariables;
	
	class PcInfo{
		
		private String name;
		private String ipAdress;
		
		public PcInfo(String name, String ipAdress) {
			super();
			this.name = name;
			this.ipAdress = ipAdress;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getIpAdress() {
			return ipAdress;
		}
		public void setIpAdress(String ipAdress) {
			this.ipAdress = ipAdress;
		}
		
		
	}
	
	@Override
     public void onCreate(Bundle savedInstanceState) {         

        super.onCreate(savedInstanceState);    
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR); // Add this line
        
        setContentView(R.layout.sc_pcslist);
        
        //getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.id.txtPC);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
       
        ActionBar actionBar = getActionBar();
        actionBar.show();
        
        
        me = this;
        mHandler = new Handler();
        //rest of the code
        //ListView listPcs =  (ListView) findViewById(R.id.listPcs);	
        
        ListView lstView = getListView();
        lstView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);        
        lstView.setTextFilterEnabled(true);
        
        refreshingDialog=new ProgressDialog(this);
        refreshingDialog.setCancelable(true);
        refreshingDialog.setMessage("Searching Pcs ....");
        refreshingDialog.setCancelable(false);
        refreshingDialog.setCanceledOnTouchOutside(false);
        refreshingDialog.setOnCancelListener(new Dialog.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // DO SOME STUFF HERE
            }
        });
        
        RefreshList();
        
        
   	 Button refresh = (Button) findViewById(R.id.btnLoadPcs);
   	refresh.setBackgroundResource(R.drawable.actualizar_selector);
   	refresh.setOnClickListener(new View.OnClickListener() {
			   public void onClick(View view) {
				   RefreshList();
			   }
			  });
    }
	
	ArrayList<ServerSocket> sockets = new ArrayList<ServerSocket>(); 
	ArrayList<String> probablyItems;
	int timeOut = 5000;

	private void RefreshList(){
		refreshingDialog.show();		
		
		
//		EditText myEditText  = (EditText) findViewById(R.id.txtTimeOut);	
//        timeOut = Integer.parseInt(myEditText.getText().toString());
        
		if(sockets != null){
			for(ServerSocket s : sockets){
				try {
					s.close();
				} catch (IOException e) {
					Log.e("ON CLOSE", e.toString());
				}
			}
		}
		
        items = new ArrayList<PcInfo>(); 
        
       probablyItems = new ArrayList<String>(); 
        sockets.clear(); 
        
        
        
        
        WifiManager manager = (WifiManager) super.getSystemService(WIFI_SERVICE);
        DhcpInfo dhcp = manager.getDhcpInfo();
        String address = Formatter.formatIpAddress(dhcp.gateway);
        probablyItems.add(address);
        
        WifiInfo myWifiInfo = manager.getConnectionInfo();
        int ipAddress = myWifiInfo.getIpAddress();
        String myIpv4 = Formatter.formatIpAddress(ipAddress);
        
        Log.d("MYIP", myIpv4);
        
        String ipv4 = null;
		//Hard Coded IP for it to work on Jorge's PC
		//probablyItems.add(192.168.0.104);
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();

			          // for getting IPV4 format
			          if (!inetAddress.isLoopbackAddress() 
			        		  && InetAddressUtils.isIPv4Address(ipv4 = inetAddress.getHostAddress())) {
			        	  if(!ipv4.equals(myIpv4))			        	 
			        		  probablyItems.add(ipv4 );//"TOLEAVE - PC: " +  
			           }
                }
            }
        } catch (Exception ex) {
        	Log.e("ServerActivity", ex.toString());
     	   //TextView view = (TextView) findViewById(R.id.txtPC);
     		//view.setText(ex.toString());
        }
        
        
        new Thread( new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				ServerSocket serverSocketTest = null;
				Socket workerSocket = null;
				
				 for(String itemIp : probablyItems){
			        	try{
				        	Log.e("IPs", itemIp);
				        	 Socket socketTest = new Socket();
				        	 socketTest.setReuseAddress(true);
				        	 socketTest.connect((new InetSocketAddress(itemIp, 10000)),timeOut);
				        	 //sockets.add(socketTest);
				        	 //Asking for the name
				        	 OutputStream os = socketTest.getOutputStream();
				        	 BufferedOutputStream outputStream = new BufferedOutputStream(os);
							 byte[] buffer = ("NAMEPC").getBytes();
							 outputStream.write(buffer, 0, buffer.length);				
							 outputStream.flush();
							 socketTest.close();
							 //End asking for the game
							 
							 //Waiting for the name
							 serverSocketTest = new ServerSocket();
							 sockets.add(serverSocketTest);
							 serverSocketTest.setReuseAddress(true);
							 serverSocketTest.bind(new InetSocketAddress(10000));
							 workerSocket = serverSocketTest.accept();
							  // When data are accepted socketInputStream will be invoked.
							 DataInputStream   socketInputStream = new DataInputStream(
							                 workerSocket.getInputStream());	
						     String namePC = null;
				            namePC = socketInputStream.readLine();
						     socketInputStream.close();
						     workerSocket.close();
						     serverSocketTest.close();
						     sockets.remove(serverSocketTest);
							 //End Waiting for the name		       	  		
		       	  			items.add( new PcInfo(namePC , itemIp));
		       	  		
			       	  	//socketTest.close();
			        	}catch(Exception e){
			        		Log.e("Retreiving Thread", e.toString());
			        	}
			        }
				 
				 mHandler.post(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							ListAdapter = new ArrayPcAvailableAdapter(me,android.R.layout.simple_list_item_checked, items);
					        me.setListAdapter(ListAdapter);
					        refreshingDialog.dismiss();
						}
					});
				 
			        
			}
		}).start();
        
       
        
        
        
       
	}
	
	ArrayAdapter<String> ListAdapter;
	 private AvailablePcs me;
	 Handler mHandler;
	 
	 @Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		ConnectPC(items.get(position).ipAdress);
	}
	 
	 private void ConnectPC(String ip){
		
		 
		//TextView view = (TextView) findViewById(R.id.txtPC);
		//view.setText(ip);	
		globalVariables = ((ToLeaveRemoteVariables)getApplication());
        globalVariables.setCommandSender(new CommandSender(this, ip, 10000, timeOut));
	   
	   final CommandSender commandSender = globalVariables.getCommandSender();
	   try {
		   commandSender.Start();
		} catch (Exception e) {
			Log.d("Failed Command Sender", e.getMessage());
		}
	   final String ipAdd = ip;
		try {			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					while(!commandSender.IsActive()){
						if(commandSender.IsFailed()){
							return;
						}
					}
					Intent intent = new Intent(me, ConnectionSuccess.class);
					intent.putExtra("ip_adress",ipAdd);
		            me.startActivity(intent);      
		            me.finish();
				}
			}).start();
		   } catch (Exception e) {
				Log.d("Failed Open Controller", e.getMessage());
		   }
	 }

	 
	  @Override
			protected void onDestroy() {
				// TODO Auto-generated method stub
				super.onDestroy();
				
			}
	  
	  
	  public class ArrayPcAvailableAdapter extends ArrayAdapter<String> {
		  private Context context;
		  private  ArrayList<PcInfo> values;
		  private int resource;

		  public ArrayPcAvailableAdapter(Context context, int resource,  ArrayList<PcInfo> values) {
		    super(context, resource);
		    this.context = context;
		    this.values = values;
		    this.resource = resource;
		    //this.setDropDownViewResource(resource);
		  }
		  	
		  
		  @Override
		  public long getItemId(int position) {
			  return position;
		  }
		 
		  @Override
		  public String getItem(int position) {
			  //return "TO LEAVE - TERMINAL: " + values.get(position).name;
			  PcInfo pcInfo = values.get(position);
			  return pcInfo.name + " - IP: "+ pcInfo.ipAdress;
		  }
		  
		  @Override		  
		  public int getCount() {
			  return values.size();
		  }
		  
		  
		  @Override
		   public View getView(int position, View convertView, ViewGroup parent)
		   {
			  View view = null;
			  LayoutInflater inflater = (LayoutInflater) context
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			  view = inflater.inflate(this.resource, parent, false);
			  
			  // Set data into the view.
		      ((TextView)view.findViewById(android.R.id.text1)).setText(getItem(position));
		    
		       return view;
		   }
		} 
	  
	  
	  @Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	      MenuInflater inflater = getMenuInflater();
	      inflater.inflate(R.menu.menu_connection, menu);
	      return true;
	  }
	  
	  @Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	      // Handle item selection
	      switch (item.getItemId()) {
	          case R.id.itemMenuTimeOut:
	        	  ChangeTimeOut();
	              return true;
	          case R.id.itemMenuManualConnection:
	        	  DoManualConnection();
	              return true;
	          default:
	              return super.onOptionsItemSelected(item);
	      }
	  }
	  
	  private void ChangeTimeOut(){
		  final EditText input = new EditText(this);
		  input.setText(timeOut + "");
		  //input.setInputType(InputType.TYPE_NUMBER_VARIATION_NORMAL);
		  
		  input.setFilters(new InputFilter[] {
				    // Maximum 2 characters.
				    new InputFilter.LengthFilter(6),
				    // Digits only.
				    DigitsKeyListener.getInstance(),  // Not strictly needed, IMHO.
				});

		// Digits only & use numeric soft-keyboard.
		input.setKeyListener(DigitsKeyListener.getInstance());
		  
		  
		AlertDialog.Builder timeOutDialog =  new AlertDialog.Builder(AvailablePcs.this)
		    .setTitle("Cambio de Time Out")
		    .setMessage("Ingrese un nuevo tiempo limite (en milisegundos) para la busqueda de las PCs disponibles")
		    .setView(input)
		    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		            timeOut = Integer.parseInt(input.getText().toString());
		            globalVariables.getCommandSender().setTimeOut(timeOut);
		        }
		    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		            // Do nothing.
		        }
		    });
		timeOutDialog.show();
		
	  }
	  
	  private void DoManualConnection(){
		  final EditText input = new EditText(this);	
		  InputFilter[] filters = new InputFilter[1];
		    filters[0] = new InputFilter() {
		        @Override
		        public CharSequence filter(CharSequence source, int start, int end,
		                android.text.Spanned dest, int dstart, int dend) {
		            if (end > start) {
		                String destTxt = dest.toString();
		                String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
		                if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) { 
		                    return "";
		                } else {
		                    String[] splits = resultingTxt.split("\\.");
		                    for (int i=0; i<splits.length; i++) {
		                        if (Integer.valueOf(splits[i]) > 255) {
		                            return "";
		                        }
		                    }
		                }
		            }
		            return null;
		        }

		    };
		    input.setFilters(filters);		 
		// Digits only & use numeric soft-keyboard.
		//input.setKeyListener(DigitsKeyListener.getInstance());
		    input.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		  
		AlertDialog.Builder manualConnectionDialog =  new AlertDialog.Builder(AvailablePcs.this)
		    .setTitle("Conexion Manual")
		    .setMessage("Ingrese la IP de la PC con la cual se desea conectar")
		    .setView(input)
		    .setPositiveButton("Conectar", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		        	ConnectPC(input.getText().toString());
		        }
		    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		            // Do nothing.
		        }
		    });
		manualConnectionDialog.show();
	  }

}
