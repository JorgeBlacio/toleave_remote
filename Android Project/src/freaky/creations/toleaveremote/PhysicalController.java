package freaky.creations.toleaveremote;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import network.*;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PhysicalController extends Activity {
	//Begin: Commands		
	
		
	    public static final String Press_A = "0";
	    public static final String Release_A = "1";
	    public static final String Press_X = "2";
	    public static final String Release_X = "3";
	    
	    public static final String Press_Left = "4";
	    public static final String Release_Left = "5";
	    public static final String Press_Right = "6";
	    public static final String Release_Right = "7";
	    
	    public static final String Press_PauseResume = "8";
	    public static final String Release_PauseResume = "9";
	    
	    public static final String Press_Up = "a";
	    public static final String Release_Up = "b";
	    public static final String Press_Down = "c";
	    public static final String Release_Down = "d";
	    
	    public static final String Impulse = "i";
	  //Important: If a new command is added, this must be with one only character different to what were used.
	
		//End: Enum Commands
		
		//Begin: Private Data Members
		private ToLeaveRemoteVariables globalVariables;
		private CommandSender commandSenderMainButtons;
		private AudioStreaming audioStreaming;
		//private CommandSender commandSenderStick;
		
		private Button buttonX;
		private Button buttonA;
		private Button buttonLeft;
		private Button buttonRight;
		private Button buttonUp;
		private Button buttonDown;
		private Button buttonPauseResume;
		
	    private Vibrator myVib;
		
		//End: Private Data Members
	    //joy-stick
		private RelativeLayout layout_joystick;
		private ImageView image_joystick, image_border;
		private TextView textView1, textView2, textView3, textView4, textView5;
		
		private JoyStickClass js;
		
		private enum MovementActions{
			LEFT, RIGHT, UP, DOWN, NONE
		}
		
		private MovementActions lastAction = MovementActions.NONE;
	
	 @Override
     public void onCreate(Bundle savedInstanceState) {         

        super.onCreate(savedInstanceState);    
        
        myVib = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        setContentView(R.layout.sc_controller);
        SetListenerEvents();	
        initialization();
    }
	 
	 
	 
	 private void initialization(){
		 try {
			myVib.vibrate(50);			
		    //commandSenderMainButtons = new CommandSender(ipAdress, 10000); 
			//commandSenderMainButtons.Start();
			globalVariables = ((ToLeaveRemoteVariables)getApplication());
			commandSenderMainButtons = globalVariables.getCommandSender();
			audioStreaming = globalVariables.getAudioStreaming();
		   } catch (Exception e) {
				Log.d("Failed Connect", e.getMessage());
		   }
	 }
	 
	 private void CheckForRelease(MovementActions firedState){
		 if(lastAction != firedState){
			 switch (lastAction) {
				case NONE:
					Log.d("Stick Message", "Last Was NONE");
					break;
				case LEFT:
					Log.d("Stick Message", "Release Left");
					commandSenderMainButtons.SendCommand(Release_Left);
					break;
				case RIGHT:
					Log.d("Stick Message", "Release Right");
					commandSenderMainButtons.SendCommand(Release_Right);
					break;
				case UP:
					Log.d("Stick Message", "Release Up");
					commandSenderMainButtons.SendCommand(Release_Up);
					break;
				case DOWN:
					Log.d("Stick Message", "Release Down");
					commandSenderMainButtons.SendCommand(Release_Down);
					break;
			}
			 
			if(firedState != MovementActions.NONE){
				switch (firedState) {
					case LEFT:
						Log.d("Stick Message", "Pressed Left");
						commandSenderMainButtons.SendCommand(Press_Left);
						break;
					case RIGHT:
						Log.d("Stick Message", "Pressed Right");
						commandSenderMainButtons.SendCommand(Press_Right);
						break;
					case UP:
						Log.d("Stick Message", "Pressed Up");
						commandSenderMainButtons.SendCommand(Press_Up);
						break;
					case DOWN:
						Log.d("Stick Message", "Pressed Down");
						commandSenderMainButtons.SendCommand(Press_Down);
						break;
				}
			}else
				Log.d("Stick Message", "Sent NONE");
			lastAction = firedState;
		 }
	 }
	//Begin: Private Member Functions
	 
	 private void SetListenerEvents(){
       
	    layout_joystick = (RelativeLayout)findViewById(R.id.layout_joystick);

	    js = new JoyStickClass(getApplicationContext()
       		, layout_joystick, R.drawable.onscreen_control_knob);
	    js.setStickSize(200, 200);
	    js.setLayoutSize(450, 450);
	    js.setLayoutAlpha(150);
	    js.setStickAlpha(255);
	    js.setOffset(80);
	    js.setMinimumDistance(50);
	    
	    layout_joystick.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				//myVib.vibrate(50);
				js.drawStick(arg1);
				if(arg1.getAction() == MotionEvent.ACTION_DOWN
						|| arg1.getAction() == MotionEvent.ACTION_MOVE) {
					
					int direction = js.get8Direction();
					if(direction == JoyStickClass.STICK_UP) {
						CheckForRelease(MovementActions.UP);
					} else if(direction == JoyStickClass.STICK_UPRIGHT) {
						CheckForRelease(MovementActions.RIGHT);
					} else if(direction == JoyStickClass.STICK_RIGHT) {
						CheckForRelease(MovementActions.RIGHT);
					} else if(direction == JoyStickClass.STICK_DOWNRIGHT) {
						CheckForRelease(MovementActions.RIGHT);
					} else if(direction == JoyStickClass.STICK_DOWN) {
						CheckForRelease(MovementActions.DOWN);
					} else if(direction == JoyStickClass.STICK_DOWNLEFT) {
						CheckForRelease(MovementActions.LEFT);
					} else if(direction == JoyStickClass.STICK_LEFT) {
						CheckForRelease(MovementActions.LEFT);
					} else if(direction == JoyStickClass.STICK_UPLEFT) {
						CheckForRelease(MovementActions.LEFT);
					} else if(direction == JoyStickClass.STICK_NONE) {
						CheckForRelease(MovementActions.NONE);
					}
				} else if(arg1.getAction() == MotionEvent.ACTION_UP) {
					CheckForRelease(MovementActions.NONE);
				}
				return true;
			}
       });
		 
		 
		 // Button A
		buttonA = (Button) findViewById(R.id.btnA);		
		buttonA.setBackgroundResource(R.drawable.a_button_selector);
		buttonA.setOnClickListener(new View.OnClickListener() {
			   public void onClick(View view) {
				   commandSenderMainButtons.SendCommand(Release_A);
			   }
			  });		
		buttonA.setOnTouchListener(new OnTouchListener() {        
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    	if(event.getAction() == MotionEvent.ACTION_DOWN){
		    		myVib.vibrate(50);
		    		commandSenderMainButtons.SendCommand(Press_A);
		    		rectA = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
		    	}
		   	    if(event.getAction() == MotionEvent.ACTION_MOVE){
	    	        if(!rectA.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){
	    	            // User moved outside bounds
	    	        	commandSenderMainButtons.SendCommand(Release_A);
	    	        }
		    	 }
		      return false;
		    }
		});
		
		
		// Button X
		buttonX = (Button) findViewById(R.id.btnX);
		buttonX.setBackgroundResource(R.drawable.x_button_selector);
		buttonX.setOnClickListener(new View.OnClickListener() {
			   public void onClick(View view) {
				   commandSenderMainButtons.SendCommand(Release_X);
			   }
			  });		
		buttonX.setOnTouchListener(new OnTouchListener() {        
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    	if(event.getAction() == MotionEvent.ACTION_DOWN){
		    		myVib.vibrate(50);
		    		commandSenderMainButtons.SendCommand(Press_X);
		    		rectX = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
		    	}
		   	    if(event.getAction() == MotionEvent.ACTION_MOVE){
	    	        if(!rectX.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){
	    	            // User moved outside bounds
	    	        	commandSenderMainButtons.SendCommand(Release_X);
	    	        }
		    	 }
		      return false;
		    }
		});
		
		
//		// Button Left
//		buttonLeft = (Button) findViewById(R.id.btnLeft);
//		buttonLeft.setBackgroundResource(R.drawable.left_button_selector);
//		buttonLeft.setOnClickListener(new View.OnClickListener() {
//			   public void onClick(View view) {
//				   commandSenderMainButtons.SendCommand(Release_Left);
//			   }
//			  });	
//		buttonLeft.setOnTouchListener(new OnTouchListener() {        
//		    @Override
//		    public boolean onTouch(View v, MotionEvent event) {
//		    	if(event.getAction() == MotionEvent.ACTION_DOWN){
//		    		myVib.vibrate(50);
//		    		commandSenderMainButtons.SendCommand(Press_Left);
//		    		rectLeft = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
//		    	}
//		   	    if(event.getAction() == MotionEvent.ACTION_MOVE){
//	    	        if(!rectLeft.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){
//	    	            // User moved outside bounds
//	    	        	commandSenderMainButtons.SendCommand(Release_Left);
//	    	        }
//		    	 }
//		      return false;
//		    }
//		});
//		
//		
//		// Button Right
//		buttonRight = (Button) findViewById(R.id.btnRight);
//		buttonRight.setBackgroundResource(R.drawable.right_button_selector);
//		buttonRight.setOnClickListener(new View.OnClickListener() {
//			   public void onClick(View view) {
//				   commandSenderMainButtons.SendCommand(Release_Right);
//			   }
//			  });		
//		buttonRight.setOnTouchListener(new OnTouchListener() {        
//		    @Override
//		    public boolean onTouch(View v, MotionEvent event) {
//		    	if(event.getAction() == MotionEvent.ACTION_DOWN){
//		    		myVib.vibrate(50);
//		    		commandSenderMainButtons.SendCommand(Press_Right);	
//		    		rectRight = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
//		    	}
//		   	    if(event.getAction() == MotionEvent.ACTION_MOVE){
//	    	        if(!rectRight.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){
//	    	            // User moved outside bounds
//	    	        	commandSenderMainButtons.SendCommand(Release_Right);
//	    	        }
//		    	 }
//		      return false;
//		    }
//		});
//		
//		// Button Up
//				buttonUp = (Button) findViewById(R.id.btnUp);
//				buttonUp.setBackgroundResource(R.drawable.up_button_selector);
//				buttonUp.setOnClickListener(new View.OnClickListener() {
//					   public void onClick(View view) {
//						   commandSenderMainButtons.SendCommand(Release_Up);
//					   }
//					  });		
//				buttonUp.setOnTouchListener(new OnTouchListener() {        
//				    @Override
//				    public boolean onTouch(View v, MotionEvent event) {
//				    	if(event.getAction() == MotionEvent.ACTION_DOWN){
//				    		myVib.vibrate(50);
//				    		commandSenderMainButtons.SendCommand(Press_Up);	
//				    		rectUp = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
//				    	}
//				   	    if(event.getAction() == MotionEvent.ACTION_MOVE){
//			    	        if(!rectUp.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){
//			    	            // User moved outside bounds
//			    	        	commandSenderMainButtons.SendCommand(Release_Up);
//			    	        }
//				    	 }
//				      return false;
//				    }
//				});
//				
//			// Button Down
//				buttonDown = (Button) findViewById(R.id.btnDown);
//				buttonDown.setBackgroundResource(R.drawable.down_button_selector);
//				buttonDown.setOnClickListener(new View.OnClickListener() {
//					   public void onClick(View view) {
//						   commandSenderMainButtons.SendCommand(Release_Down);
//					   }
//					  });		
//				buttonDown.setOnTouchListener(new OnTouchListener() {        
//				    @Override
//				    public boolean onTouch(View v, MotionEvent event) {
//				    	if(event.getAction() == MotionEvent.ACTION_DOWN){
//				    		myVib.vibrate(50);
//				    		commandSenderMainButtons.SendCommand(Press_Down);	
//				    		rectDown = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
//				    	}
//				   	    if(event.getAction() == MotionEvent.ACTION_MOVE){
//			    	        if(!rectDown.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){
//			    	            // User moved outside bounds
//			    	        	commandSenderMainButtons.SendCommand(Release_Down);
//			    	        }
//				    	 }
//				      return false;
//				    }
//				});
				
				// Button Pause Resume
				buttonPauseResume = (Button) findViewById(R.id.btnPauseResume);
				buttonPauseResume.setBackgroundResource(R.drawable.start_button_selector);
				buttonPauseResume.setOnClickListener(new View.OnClickListener() {
					   public void onClick(View view) {
						   commandSenderMainButtons.SendCommand(Release_PauseResume);
					   }
					  });		
				buttonPauseResume.setOnTouchListener(new OnTouchListener() {        
				    @Override
				    public boolean onTouch(View v, MotionEvent event) {
				    	if(event.getAction() == MotionEvent.ACTION_DOWN){
				    		myVib.vibrate(50);
				    		commandSenderMainButtons.SendCommand(Press_PauseResume);	
				    		rectPauseResume = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
				    	}
				   	    if(event.getAction() == MotionEvent.ACTION_MOVE){
			    	        if(!rectPauseResume.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){
			    	            // User moved outside bounds
			    	        	commandSenderMainButtons.SendCommand(Release_PauseResume);
			    	        }
				    	 }
				      return false;
				    }
				});
	 }
	 
	 private Rect rectLeft;
	 private Rect rectRight;
	 private Rect rectA;
	 private Rect rectX;
	 private Rect rectUp;
	 private Rect rectDown;
	 private Rect rectPauseResume;
	 
	 //End
	 
	
	  @Override
	  protected void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
			try {
				
				commandSenderMainButtons.SendCommand(AudioStreaming.DISMISS_STREAMING);	
				while(commandSenderMainButtons.IsSending()){}
				commandSenderMainButtons.Stop();
				//Log.e("OUT", "dfgjd");
				
			} catch (Exception e) {
				Log.d("Failed Stop", e.getMessage());
			}
			
		}
	  
	  @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			if(audioStreaming != null && audioStreaming.IsPlaying())
				audioStreaming.Pause();
			
		} catch (Exception e) {
			Log.d("Failed in Streamer", e.getMessage());
		}
	}
	  
	  @Override
		protected void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			try {
				if(audioStreaming != null && !audioStreaming.IsPlaying())
					audioStreaming.Resume();				
			} catch (Exception e) {
				Log.d("Failed in Streamer", e.getMessage());
			}
		}
}
