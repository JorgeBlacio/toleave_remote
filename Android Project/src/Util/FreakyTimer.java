package Util;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Handler;
import android.util.Log;

public class FreakyTimer {
	
	private ActionOnTimer actionOnTimer;
	private double secondsToCount;
	
	public FreakyTimer(double seconds, ActionOnTimer actionTimer){
		actionOnTimer = actionTimer;
		secondsToCount = seconds;
	}
	
	Handler handler = new Handler(); 
	Timer timer = new Timer();
	Runnable doA = new Runnable() {
	    @Override
	    public void run() {
	    	actionOnTimer.DoAction();
	    }
	};
	
	

	public void Execute(){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                handler.post(doA);
            }
        };
        long ml = (long)(secondsToCount * 1000);
        Log.d("lOL", ""+ ml);
        timer.schedule(task, ml); 	    
	}
	
	public interface ActionOnTimer{
		void DoAction();
	}
}
